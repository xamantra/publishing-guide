# Step 1
- Go to the codemagic project: [https://codemagic.io/app/5ee86d5a1a9bec18e39fb49a/workflow/5ee86d5a1a9bec18e39fb499/settings](https://codemagic.io/app/5ee86d5a1a9bec18e39fb49a/workflow/5ee86d5a1a9bec18e39fb499/settings)
- Select `ios-release`
- Click `Start new build`

    ![step 1](./images/cm/001.png)

<hr>

# Step 2
- Select `btx_ios`
- Click `Start new build`

    ![step 2](./images/cm/002.png)

<hr>

# Step 3
- The build will start a list of process.
- Wait until all these process are done.

    ![step 3](./images/cm/003.png)

- When the processes are done, go to `Step 4`

    ![step 3](./images/cm/004.png)

<hr>

# Step 4
- Go to appstore connect: [https://appstoreconnect.apple.com/apps/1491695409/appstore/info](https://appstoreconnect.apple.com/apps/1491695409/appstore/info)

    ![step 4](./images/cm/005.png)

<hr>

# Step 5
- Go to `TestFlight`
- Click the *yellow warning* sign.
- Click `Provide Export Compliance Information`
- **NOTE:** The yellow warning sign will appear after few minutes or sometimes an hour. You have to wait for it.

    ![step 5](./images/cm/006.png)

<hr>

# Step 6
- Select `No`
- Click `Start Internal Testing`

    ![step 6](./images/cm/007.png)

<hr>

# Step 7
- Go back to codemagic on this link [https://codemagic.io/app/5ee86d5a1a9bec18e39fb49a](https://codemagic.io/app/5ee86d5a1a9bec18e39fb49a)
- Remember the version of latest build: `3.9.30`
- Now go to `Step 8`

    ![step 7](./images/cm/009.png)

<hr>

# Step 8
- Go to `App Store` tab.
- Click `+ VERSION OR PLATFORM`
- Select `iOS`

    ![step 8](./images/cm/010.png)

<hr>

# Step 9
- Set the version based on the build earlier on `Step 7`.
- Click `Create`

    ![step 9](./images/cm/011.png)

<hr>

# Step 10
- Edit `What's New in This Version?`

    ![step 10](./images/cm/012.png)

<hr>

# Step 11
- On the same page, click `Select a build before you submit your app.`

    ![step 11](./images/cm/013.png)

<hr>

# Step 12
- Select the version you built earlier.
- Click `Done`.

    ![step 12](./images/cm/014.png)

<hr>

# Step 13
- Click `Save`

    ![step 13](./images/cm/016.png)

<hr>

# Step 14
- Click `Submit for Review`

    ![step 14](./images/cm/017.png)

<hr>

# Step 15
- Select `No`
- Click `Submit`

    ![step 15](./images/cm/018.png)

<hr>

# Final Step
- Wait for several hours for AppStore to approve the release.

    ![step 16](./images/cm/019.png)