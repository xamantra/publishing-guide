## Step 1
Go to this link: https://play.google.com/apps/publish

<hr>

## Step 2
Select the app from the list:

![Select the app from the list](./images/001.png)

<hr>

## Step 3
Click `Release management`:

![Click Release management](./images/002.png)

<hr>

## Step 4
Click `App releases`:

![Click App releases](./images/003.png)

<hr>

## Step 5
Click `Manage`:

![Click Manage](./images/004.png)

<hr>

## Step 6
Click `CREATE RELEASE`:

![Click CREATE RELEASE](./images/005.png)

<hr>

## Step 7
Click `BROWSE FILES`:
 
![Click BROWSE FILES](./images/006.png)

**Note:** You can also drag and drop the file.

<hr>

## Step 8:
Select the `.apk` or `.aab` file, the filename can be anything:

![Select the .apk or .aab file](./images/007.png)

<hr>

## Step 9
Wait for the upload to finish:

![Wait for the upload to finish](./images/008.png)

<hr>

## Step 10
Edit release notes, click `SAVE` and click `REVIEW`:

![Edit release notes, click SAVE and click REVIEW](./images/009.png)

<hr>

## Final Step
Click `START ROLL-OUT TO PRODUCTION`:

![Click START ROLL-OUT TO PRODUCTION](./images/010.png)

Wait for several hours for google play to approve the release.